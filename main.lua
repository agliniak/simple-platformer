--AJ Gliniak 2015
require "rectangle"
require "utilities"
require "player"
require "eventHandler"
require "collisionHandler"
require "base"
require "levelManager"
require "inputHandler"
require "app"
require "base"
--require "playerOLD"
require "vector2d"
require "hitBox"
require "mover"
require "platform"

function love.load()

	app = App:new()
	app:load()
end

function love.draw()

	app:draw()
end

function love.update(dt)
	
	app:update(dt)
end

