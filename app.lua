App = {}

function App:new()
	local newApp = {}
	setmetatable(newApp, self)
	self.__index = self


	--load controls
	--newApp.controls = {}

	--newApp.inputHandler = InputHandler:new()--load current keyboard controls
	-- newApp.player = Player:new(200,300)
	--newApp.levelManager = LevelManager:new()
	-- newApp.currentLevel = newApp.levelManager:loadLevel("level0.lua")
	--make collisionHandler(levelrects)

	return newApp
end

function App:load()
	--started to move items during creation of app into load. I dont need this for all objects,
	--i might move to this tho, seems more elegant also look at how you have to do control loading
	--if its in new() vs load()  in load i can use self.   in new() i have to use newapp,
	
	self.controls = self:loadControls()--move this intoinputhandler?
	self.levelManager = LevelManager:new()
	self.currentLevel = self.levelManager:loadLevel("level0.lua")

	--self.player = PlayerOLD:new(200,300)--insert levelspawn for cords getLevelSpawn()
	self.inputHandler = InputHandler:new(self.controls)



	--playerV2 test
	self.player = Player:new()
	self.player:setSpawn(200,300)


	self.collisionHandler = CollisionHandler:new(self.player,self.currentLevel)


	self.platform = Platform:new()


end

function App:loadControls()
	
	return love.filesystem.load("controls.lua")

end


function App:draw()
	--draw level
	for i=1,table.getn(self.currentLevel) do
		self.currentLevel[i]:draw()
	end

	--draw player
	self.platform:draw()
	self.player:draw()
	
end

function App:update(dt)
	--update level
	self.inputHandler:update()
	
	--update player and all moving things
	self.player:update(self.inputHandler:GetInput())

	--ok now that everything has moved, 
	self.collisionHandler:update()
end
