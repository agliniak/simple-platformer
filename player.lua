--player is a vector, keyboard movable, drawable, entity
Player = {}

function Player:new()
	local newPlayer = {}
	setmetatable(newPlayer, self)
	self.__index = self
	
	
	newPlayer.location = Vector2d:new(0,0)
	newPlayer.velocity = Vector2d:new(0,0)
	newPlayer.acceleration = Vector2d:new(0,0)
	newPlayer.topspeed = 3
	newPlayer.mass = 10
	newPlayer.grounded = false
	


	--newPlayer.rect = Rectangle:new(newPlayer.location.x,newPlayer.location.y,10,15,255,50,50,255,"fill")
	newPlayer.hitBox = HitBox:new(newPlayer.location.x,newPlayer.location.y,10,15,255,50,50,255,"fill")
	return newPlayer

end

function Player:draw()
	self.hitBox:draw()
	
end
--now i think moverclass might be a good idea
--to apply gravity etc
function Player:applyForce(force)

	force:divide(self.mass)
	self.acceleration:add(force)

end
 
function Player:UndoLastMove()
	self.location:subtract(self.velocity)
end

function Player:update(inputHandler)

	--check if key held, apply that
	self:checkInput(inputHandler)
	--print(self.velocity.y)

-- if self.grounded ==false then
-- 	local gravity = Vector2d:new(0,5)
-- 	self:applyForce(gravity)
-- end



	--self:applyForce( self:CheckCollision() )
	self.velocity:add(self.acceleration)


	



	self.velocity:Limit(self.topspeed)
	self.location:add(self.velocity)
	self.acceleration:multiply(0)

	self.hitBox.x=self.location.x
	--have you colided?
	--if collision  then
	--self.rect.x=selfrect.x-selflocation.x
	--

		--end
	--

--evan usualy checks y  cuz your ontop of importantce,

--check y first if airborne etc


	self.hitBox.y=self.location.y

--after final movement,  add x movemnt  check collison oh no your in a block!  invalid,  undo what you did so x plus 3 is invalid, so take curent x and subtract



	
	
end



function Player:setSpawn(x,y)

	self.location:SetVector(x,y)
end

-- function Player:CheckCollision()
	
-- end

function Player:checkInput(input)
	--make this return the vector?!?!

	if input=="a" then

		--self.velocity.x= self.velocity.x-self.acceleration.x
		local moveleft = Vector2d:new(-.2,0)
		self:applyForce(moveleft)
		return moveleft
	
	end
	if input=="d" then
			
		--self.velocity.x= self.velocity.x+self.acceleration.x
		local moveright = Vector2d:new(.2,0)
		self:applyForce(moveright)
		--return moveleft
	end
	if input=="w"then
			
		--self.velocity.y= self.velocity.y-self.acceleration.y
		local moveup = Vector2d:new(0,-.2)
		self:applyForce(moveup)
	end
	if input=="s"then
			
		--self.velocity.y= self.velocity.y+self.acceleration.y	
		local movedown = Vector2d:new(0,.2)
		self:applyForce(movedown)
	end
	if input==" " then
		local jump = Vector2d:new(0,-.6)
		self:applyForce(jump)

	end
	
	return Vector2d:new(0,0)
end
