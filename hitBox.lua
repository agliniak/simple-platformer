HitBox = {}

function HitBox:new(x,y,w,h,r,g,b,a,fill)
	local newHitBox = {}
	setmetatable(newHitBox, self)
	self.__index = self

	newHitBox.x = x or 0
	newHitBox.y = y or 0
	newHitBox.w = w or 16
	newHitBox.h = h or 16
	
	newHitBox.x2 = newHitBox.x+newHitBox.w
	newHitBox.y2 = newHitBox.y+newHitBox.h
	newHitBox.halfwidthX = newHitBox.w/2
	newHitBox.halfwidthY = newHitBox.h/2


	newHitBox.color = {}
	newHitBox.color.r = r or 255
	newHitBox.color.g = g or 255
	newHitBox.color.b = b or 255
	newHitBox.color.a = a or 255
	newHitBox.fill = fill or "line"
	newHitBox.visible = true
	newHitBox.mass = 100000 --remove after testing vector collision


	return newHitBox
end

function HitBox:getCenter()
	local center = {}
	center.x = (self.x+self.w)/2
	center.y = (self.y+self.w)/2
	return center
end

function HitBox:setColor(r,g,b,a)
 	self.color.r=r
 	self.color.g=g
 	self.color.b=b
 	self.color.a=a
end

function HitBox:draw()

	--later this will only draw if debug mode is on
	if self.visible == true then
	love.graphics.setColor(self.color.r, self.color.g, self.color.b, self.color.a)
	love.graphics.rectangle(self.fill, self.x, self.y, self.w, self.h)
	end

end
