CollisionHandler = {}

function CollisionHandler:new(player, levelRects)
	local newCollisionHandler = {}
	setmetatable(newCollisionHandler, self)
	self.__index = self

	newCollisionHandler.player = player          --the player hitbox
	newCollisionHandler.levelRects = levelRects  --all level hitboxs

	return newCollisionHandler
end

function CollisionHandler:update()

	self:checkPlayerVSLevelRects()
end

--in use
function CollisionHandler:GetRelativeVelocity(vectorA,vectorB)


	local relativeV= Vector2d:new()
	relativeV.x=vectorB.x-vectorA.x
	relativeV.y=vectorB.y-vectorA.y
	return relativeV
end

function CollisionHandler:GetPenRect(rectA,rectB)
--non minkowski
	local rectC = {}
	rectC.x1 = math.max(rectA.x,rectB.x) 
	rectC.y1 = math.max(rectA.y,rectB.y) 
	rectC.x2 = math.min(rectA.x+rectA.w,rectB.x+rectB.w) 
	rectC.y2 = math.min(rectA.y+rectA.h,rectB.y+rectB.h) 
	rectC.w = rectC.x2-rectC.x1
	rectC.h = rectC.y2-rectC.y1
	return rectC
end

-- check player hitbox against every hitbox and currently includes
function CollisionHandler:checkPlayerVSLevelRects()  
	local collision = false


	for i=1, table.getn(self.levelRects) do


		--check y collsiion first
		if CollisionHandler:AIntersectB(self.player.hitBox,self.levelRects[i]) then
			
			--find the axis of least penetration,
			--ie,  y axis is least, resolve colosion in y axis
			penRect=self:GetPenRect(self.player.hitBox,self.levelRects[i])

			--this is to draw the pen rect, not the minkowski
			--pen rect keeps growing.
			self.levelRects[i].minkowski.x=penRect.x1
			self.levelRects[i].minkowski.y=penRect.y1
			self.levelRects[i].minkowski.w=penRect.w
			self.levelRects[i].minkowski.h=penRect.h


			local playerbounce = .05
			--from top check y first
			escapeVector = {}
			escapeVector.x=0
				escapeVector.y=0

				platformVector = Vector2d:new(0,0)
				platformMass = 0


				rv=self:GetRelativeVelocity(self.player.velocity,platformVector)
				

				local penDepth=penRect.h

			
			if math.abs(self.player.hitBox.y2-self.levelRects[i].y)<math.abs(self.player.hitBox.y2-self.levelRects[i].y2) then
				escapeVector.x=0
				escapeVector.y=-1
				--penDepth=math.abs(self.player.hitBox.y2-self.levelRects[i].y)

				print("from top!="..penDepth)
				
				self.player.grounded=true
			
			else
			    self.player.grounded=false
			end
		

			--from bottem
			if math.abs(self.player.hitBox.y-self.levelRects[i].y2) < math.abs(self.player.hitBox.y-self.levelRects[i].y)  then 
				
				escapeVector.x=0
				escapeVector.y=1

				print("from bottem!")
				self.player.grounded=false
			end				
				self.player.velocity.y= 0
				--self.player.velocity.y=self.player.velocity.y*.5
				self.player.acceleration.y=0
				self.player.hitBox.y=self.player.hitBox.y+(escapeVector.y*penDepth)
		end

		--x collision
		if CollisionHandler:AIntersectB(self.player.hitBox,self.levelRects[i]) then
			
			
		end



	end
end


function CollisionHandler:GetPenVector(rectC)
	--getting the penetration vector of the minkowski rect angle and 0,0
	local origin = {}
	origin.x=0
	origin.y=0

	--object A from right
	minDist= math.abs(rectC.x-0)
	penVector=Vector2d:new(1,0) 
	--print("pen="..rectC.x)

	--if from left
	if math.abs(rectC.x2-0)< minDist then
		minDist=math.abs(rectC.x2-0)
		--print("pen="..rectC.x2)
		penVector.x=-1
		penVector.y=0

	end


	-- if math.abs(rectC.y-0)<minDist then
	-- 	minDist=rectC.y
	-- 	penVector.x=0
	-- 	penVector.y=-minDist
	-- end

	-- if math.abs(rectC.y2-0)<minDist then
	-- 	minDist=rectC.y2
	-- 	penVector.x=0
	-- 	penVector.y=minDist

	-- end


	
	--the pen vector is the minimum distance from the originto the minkowski rect.
	return penVector
end


function CollisionHandler:AIntersectB(rectA,rectB)

	--if every thing is true then there is an intersection
	if rectA.x < rectB.x+rectB.w and
   		rectA.x+rectA.w > rectB.x and
  		rectA.y < rectB.y+rectB.h and
   		rectA.y+rectA.h > rectB.y then

  		return true
	else
		
		return false
	end
end




--*************************************************************************************************************
--depreciation line, anything below is currently not used


--not used ******
-- function CollisionHandler:GetNormal(vector)

-- 	local mag = vector:magnitude()
-- 	return vector:divideout(mag)
-- end


--not used *******
-- function CollisionHandler:GetDotProduct(vectorA,vectorB)

-- 	return (vectorA.x*vectorB.x)+(vectorA.y*vectorB.y)
-- end




--not used ***********
-- function CollisionHandler:DistanceBtwCenters(rectA,rectB)

	
-- 	local platformCenter={}
-- 	local playerCenter ={}
-- 	local distanceBtwCenters ={}
-- 	playerCenter.y = (rectA.y+(rectA.y+rectA.h))/2
-- 	platformCenter.y = (rectB.y+(rectB.y+rectB.h))/2

-- 	playerCenter.x = (rectA.x+(rectA.x+rectA.w))/2
-- 	platformCenter.x = (rectB.x+(rectB.x+rectB.w))/2

-- 	distanceBtwCenters.y=playerCenter.y-platformCenter.y
-- 	distanceBtwCenters.x=playerCenter.x-platformCenter.x
-- 	--if distanceBtwCenterY is Negative then its from top
-- 	--if distanceBteCenterY is Positice then its from bottem
-- 	--id distancebtwCenterX is Negative then its from the left
-- 	--if distacebtwCenterX is Postive then its from the right

-- 	return distanceBtwCenters

-- end


--not used *********
-- function CollisionHandler:MinkowskiDifference(rectA,rectB)

-- 	local rectC = {}
-- 	rectC.x = rectA.x-(rectB.x+rectB.w)
-- 	rectC.y = rectA.y-(rectB.y+rectB.h)
-- 	rectC.w=rectA.w+rectB.w
-- 	rectC.h=rectA.h+rectB.h
-- 	rectC.x2 = rectC.x + rectC.w
-- 	rectC.y2 = rectC.y + rectC.h

-- 	return rectC
-- end


-- not used *********
-- function CollisionHandler:MinkowskiHitOrigin(rectC)

-- 	if rectC.x<=0 and
-- 		rectC.x2>=0 and
-- 		rectC.y<=0 and
-- 		rectC.y2>=0 then

	
-- 		print("minkowski Collision!")
	
-- 		return true
-- 	else
--     	return false
-- 	end
-- end


--not used ************
-- function CollisionHandler:CheckDirection(distanceBtwCenters)

-- 	if distanceBtwCenters.x < 0 then
-- 		print("From Left!")

-- 		elseif distanceBtwCenters.x > 0 then
-- 			print("From Right")

-- 	end
-- 	if distanceBtwCenters.y < 0 then
-- 		print("From Top!")

-- 		elseif distanceBtwCenters.y > 0 then
-- 			print("From Bottem")

-- 	end
-- 	--get centers,
-- 	--measure distance,
-- 	--positive and negative can help direction?



-- end



-- (-1,0) left face normal
-- (1,0) right face normal
-- (0,-1) bottem face normal
-- (0,1) top face normal